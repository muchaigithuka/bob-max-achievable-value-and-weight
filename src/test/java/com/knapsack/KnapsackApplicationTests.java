package com.knapsack;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class KnapsackApplicationTests {

	int arrayOfValues[] = new int[] {10 , 40 , 30 , 50}; //values
	int arrayOfWeights[] = new int[]{5 , 4  , 6 , 4}; //weight
	int maxWeight = 10; //max weight
	int maxValueAchievable = 90; //max value
	int lengthOfArrayOfValues = arrayOfValues.length;

	@Test
	public void testDartWithinMiddleTarget() {


		int maxLoad = KnapsackApplication.evaluateMaxValue(maxWeight , arrayOfWeights , arrayOfValues , lengthOfArrayOfValues);

		assertEquals(90, maxLoad);

	}


}
