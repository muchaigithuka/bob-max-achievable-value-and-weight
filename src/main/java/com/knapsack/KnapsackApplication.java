package com.knapsack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnapsackApplication {

	public static void main(String[] args) {


		SpringApplication.run(KnapsackApplication.class, args);
      //jo has already picked weights
		int arrayOfValues[] = new int[] {10 , 40 , 30 , 50}; //values
		int arrayOfWeights[] = new int[]{5 , 4  , 6 , 4}; //weight
		int maxWeight = 10; //max weight
		int maxValueAchievable = 90; //max value
		int lengthOfArrayOfValues = arrayOfValues.length; //size 0f listed values

		System.out.println("\n" + "\n" + "Max Value Bob Can get Given the Weight Limit is " + evaluateMaxValue(maxWeight , arrayOfWeights , arrayOfValues , lengthOfArrayOfValues));

	}

	public static int evaluateMaxValue(int W, int[] wt, int[] val, int n) {
		if(n==0 || W ==0){
			return 0;
		}
		if(wt[n-1] > W){
			//length is 4 , but we evaluate arrays from 0 ,
			//so what does -1 mean ,no element at wt[4]
			//we evaluate 4-1=3 , 3-1=2 , 2-1=1 , 1-1=0 -------->thats array of size 4 with key value pairs
			//if weight at any index is greater than 10
//			int arrayOfWeights[] = new int[]{5 , 4  , 6 , 4};
			//none of this weights is greater than 10
			//why minus 1

			return evaluateMaxValue(W , wt , val , n-1);

		}else{
			return max(val[n-1] + evaluateMaxValue(W - wt[n-1] , wt , val , n-1) , evaluateMaxValue(W ,wt ,val, n-1));
		}
	}

	public static int max(int a, int b) {
		if(a > b){
			return a;
		}else{
			return b;
		}
	}

}
